# README #
MBBC Ionic app

### How do I get set up? ###

* Steps to run project 

MBBC ionic app created by generator-ionic via yeoman : 
https://github.com/diegonetto/generator-ionic

1. install nodejs
2. install bower : npm bower -g install
3. install yeoman :  npm install -g yo
4. install generator-ionic : npm install -g generator-ionic
5. install compass : gem install compass
6. install grunt : npm install -g grunt-cli 
7. npm install grunt --save-dev


* How to do with grunt

1. npm install
2. bower install
3. bower update
4. ionic platform add ios
5. run on web: grunt serve

- run on emulator : grunt emulate:ios
- build : grunt build:ios
- run on device : grunt run:ios --device
- run on specific emulator : grunt emulate:ios --target="iPhone-6"
- test : grunt test




### Contribution guidelines ###
Create your branch and develop and test. If all testing is done push to master branch