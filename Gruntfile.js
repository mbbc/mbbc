// Generated on 2016-08-04 using generator-ionic 0.8.0
'use strict';

var _ = require('lodash');
var path = require('path');
var cordovaCli = require('cordova');
var spawn = process.platform === 'win32' ? require('win-spawn') : require('child_process').spawn;

module.exports = function(grunt) {

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Configurable paths for the application
  var appConfig = {
    app: require('./bower.json').appPath || 'app',
    dist: 'www'
  };

  // Define the configuration for all the tasks
  grunt.initConfig({

    // Project settings
    yeoman: appConfig,
    /*{
      // configurable paths
      app: 'app',
      scripts: 'scripts',
      styles: 'styles',
      images: 'images',
      test: 'test',
      dist: 'www'
    },
    */

    // Environment Variables for Angular App
    // This creates an Angular Module that can be injected via ENV
    // Add any desired constants to the ENV objects below.
    // https://github.com/diegonetto/generator-ionic/blob/master/docs/FAQ.md#how-do-i-add-constants
    /*
        ngconstant: {
          options: {
            space: '  ',
            wrap: '"use strict";\n\n {%= __ngModule %}',
            name: 'config',
            dest: '<%= yeoman.app %>/<%= yeoman.scripts %>/configuration.js'
          },
          development: {
            constants: {
              ENV: {
                name: 'development',
                apiEndpoint: 'http://dev.yoursite.com:10000/'
              }
            }
          },
          production: {
            constants: {
              ENV: {
                name: 'production',
                apiEndpoint: 'http://api.yoursite.com/'
              }
            }
          }
        },
    */
    // Watches files for changes and runs tasks based on the changed files
    watch: {
      bower: {
        files: ['bower.json'],
        tasks: ['wiredep']
      },
      js: {
        files: ['<%= yeoman.app %>/scripts/{,*/}*.js'],
        tasks: ['newer:jshint:all'],
        options: {
          livereload: '<%= connect.options.livereload %>'
        }
      },
      jsTest: {
        files: ['test/spec/{,*/}*.js'],
        tasks: ['newer:jshint:test', 'karma']
      },
      compass: {
        files: ['<%= yeoman.app %>/<%= yeoman.styles %>/**/*.{scss,sass}'],
        tasks: ['compass:server', 'autoprefixer:server']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      },
      livereload: {
        options: {
          livereload: '<%= connect.options.livereload %>'
        },
        files: [
          '<%= yeoman.app %>/{,*/}*.html',
          '<%= yeoman.app %>/styles/{,*/}*.css',
          '<%= yeoman.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
        ]
      }
    },

    // The actual grunt server settings
    connect: {
      options: {
        port: 9000,
        // Change this to '0.0.0.0' to access the server from outside.
        hostname: 'localhost',
        livereload: 35729
      },
      livereload: {
        options: {
          open: true,
          middleware: function(connect) {
            return [
              connect.static('.tmp'),
              connect().use(
                '/bower_components',
                connect.static('./app/bower_components')
              ),
              connect().use(
                '/app/styles',
                connect.static('./app/styles')
              ),
              connect().use(
                '/images/icons',
                connect.static('./app/images/icons')
              ),
              connect().use(
                '/images/Navigation_icons/',
                connect.static('./app/images/Navigation_icons/')
              ),
              connect.static(appConfig.app)
            ];
          }
        }
      },
      test: {
        options: {
          port: 9001,
          middleware: function(connect) {
            return [
              connect.static('.tmp'),
              connect.static('test'),
              connect().use(
                '/bower_components',
                connect.static('./bower_components')
              ),
              connect.static(appConfig.app)
            ];
          }
        }
      },
      dist: {
        options: {
          open: true,
          base: '<%= yeoman.dist %>'
        }
      }
    },

    // Make sure code styles are up to par and there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: {
        src: [
          'Gruntfile.js',
          '<%= yeoman.app %>/scripts/{,*/}*.js'
        ]
      },
      test: {
        options: {
          jshintrc: 'test/.jshintrc'
        },
        src: ['test/spec/{,*/}*.js']
      }
    },

    // Empties folders to start fresh
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= yeoman.dist %>/{,*/}*',
            '!<%= yeoman.dist %>/.git{,*/}*'
          ]
        }]
      },
      server: '.tmp'
    },

     // Add vendor prefixed styles
     autoprefixer: {
         options: {
           browsers: ['last 1 version']
         },
         server: {
           options: {
             map: true,
           },
           files: [{
             expand: true,
             cwd: '<%= yeoman.app %>/styles/',
             src: '{,*/}*.css',
             dest: '<%= yeoman.app %>/styles/'
           }]
         },
         dist: {
           files: [{
             expand: true,
             cwd: '<%= yeoman.app %>/styles/',
             src: '{,*/}*.css',
             dest: '<%= yeoman.app %>/styles/'
           }]
         }
       },

    // Automatically inject Bower components into the app
    wiredep: {
      app: {
        src: ['<%= yeoman.app %>/index.html'],
        ignorePath: /\.\.\//,
        exclude: []
      },
      test: {
        devDependencies: true,
        src: '<%= karma.unit.configFile %>',
        ignorePath: /\.\.\//,
        fileTypes: {
          js: {
            block: /(([\s\t]*)\/{2}\s*?bower:\s*?(\S*))(\n|\r|.)*?(\/{2}\s*endbower)/gi,
            detect: {
              js: /'(.*\.js)'/gi
            },
            replace: {
              js: '\'{{filePath}}\','
            }
          }
        }
      },
      sass: {
        src: ['<%= yeoman.app %>/styles/{,*/}*.{scss,sass}'],
        ignorePath: /(\.\.\/){1,2}bower_components\//
      }
    },


    // Compiles Sass to CSS and generates necessary files if requested
    compass: {
      options: {
        sassDir: '<%= yeoman.app %>/styles',
        cssDir: '<%= yeoman.app %>/styles',
        generatedImagesDir: '.tmp/images/generated',
        imagesDir: '<%= yeoman.app %>/images',
        javascriptsDir: '<%= yeoman.app %>/scripts',
        fontsDir: '<%= yeoman.app %>/styles/fonts',
        importPath: '<%= yeoman.app %>/bower_components',
        httpImagesPath: '/images',
        httpGeneratedImagesPath: '/images/generated',
        httpFontsPath: '/styles/fonts',
        relativeAssets: false,
        assetCacheBuster: false,
        raw: 'Sass::Script::Number.precision = 10\n'
      },
      dist: {
        options: {
          generatedImagesDir: '<%= yeoman.dist %>/images/generated'
        }
      },
      server: {
        options: {
          sourcemap: true
        }
      }
    },

    //Renames files for browser caching purposes
   filerev: {
     dist :{
       src: [
         '<%= yeoman.dist %>/scripts/{,*/}*.js',
         '<%= yeoman.dist %>/styles/{,*/}*.css'
       ]
     }
   },


    // Reads HTML for usemin blocks to enable smart builds that automatically
    // concat, minify and revision files. Creates configurations in memory so
    // additional tasks can operate on them
    useminPrepare: {
     html: '<%= yeoman.app %>/index.html',
     options: {
       dest: '<%= yeoman.dist %>',
       flow: {
         html: {
           steps: {
             js: ['concat', 'uglifyjs'],
             css: ['concat']//,'cssmin'
           },
           post: {}
         }
       }
     }
   },

    // Performs rewrites based on the useminPrepare configuration
    usemin: {
      html: ['<%= yeoman.dist %>/index.html'],
      css: ['<%= yeoman.dist %>/styles/{,*/}*.css'],
      js: ['<%= yeoman.dist %>/scripts/{,*/}*.js'],
      options: {
        assetsDirs: [
          '<%= yeoman.dist %>',
          '<%= yeoman.dist %>/images',
          '<%= yeoman.dist %>/styles',
          '<%= yeoman.dist %>/fonts'
        ],
        patterns: {
          js: [
            [/(images\/[^''""]*\.(png|jpg|jpeg|gif|webp|svg))/g, 'Replacing references to images']
          ]
        }
      }

    },

    // The following *-min tasks produce minified files in the dist folder
    // cssmin: {
    //   options: {
    //     //root: '<%= yeoman.app %>',
    //     noRebase: true
    //   }
    // },
    htmlmin: {
      dist: {
        options: {
          collapseWhitespace: true,
          collapseBooleanAttributes: true,
          removeCommentsFromCDATA: true,
          removeOptionalTags: true
        },
        files: [{
          expand: true,
          cwd: '<%= yeoman.dist %>',
          src: [
            '*.html',
            './**/*.html'
          ],
          dest: '<%= yeoman.dist %>'
        }]
      }
    },

    // ng-annotate tries to make the code safe for minification automatically
    // by using the Angular long form for dependency injection.
    ngAnnotate: {
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/concat/scripts',
          src: '*.js',
          dest: '.tmp/concat/scripts'
        }]
      }
    },


    // Copies remaining files to places other tasks can use
    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= yeoman.app %>',
          dest: '<%= yeoman.dist %>',
          src: [
            //'<%= yeoman.images %>/**/*.{png,jpg,jpeg,gif,webp,svg}',
            '*.html',
            'scripts/**/*.html',
            'scripts/**/*.js',
            'assets/images/*.{png,jpg,ico,jpeg,gif,webp,svg}',
            'assets/images/icons/*.{png,jpg,ico,jpeg,gif,webp,svg}',
            'assets/fonts/*.{eot,ijmap,svg,ttf,woff,woff2}',
            'assets/styles/*.{css, scss}'
          ]
        }, {
          expand: true,
          cwd: '.temp/<%= yeoman.images %>',
          dest: '<%= yeoman.dist %>/images',
          src: ['generated/*']
        }, {
          expand: true,
          cwd: '.',
          dest: '<%= yeoman.dist %>',
          src: ['resources/**/*']
        }, {
          expand: true,
          cwd: './app/assets/',
          dest: '<%= yeoman.dist %>/',
          src: ['fonts/*']
        }, {
          expand: true,
          cwd: './app/assets/images/',
          dest: '<%= yeoman.dist %>/',
          src: ['images/*']
        }, {
          expand: true,
          cwd: '<%= yeoman.app %>/bower_components/ionic/release/fonts/',
          dest: '<%= yeoman.dist %>/fonts/',
          src: '*'
        }]
      },

      styles: {
        expand: true,
        cwd: '<%= yeoman.app %>/assets/styles',
        dest: '<%= yeoman.dist %>/assets/styles',
        src: '{,*/}*.css'
      },
      fonts: {
        expand: true,
        cwd: '<%= yeoman.app %>/bower_components/ionic/release/fonts/',
        dest: '<%= yeoman.dist %>/fonts/',
        src: '*'
      }
    },

    concurrent: {
      ionic: {
        tasks: [],
        options: {
          logConcurrentOutput: true
        }
      },
      server: [
        'compass:server'
      ],
      test: [
        'compass'
      ],
      dist: [
        'compass:dist'
      ]
    },

    // Test settings
    karma: {
      unit: {
        configFile: 'test/karma.conf.js',
        singleRun: false
      }
    },
/*
    conventionalChangelog: {
      options: {
        changelogOpts: {
          preset: 'angular'
        }
      },
      release: {
        src: 'CHANGELOG.md'
      }
    },
    */
    bump: {
      options: {
        files: [
          'package.json',
          'bower.json'
        ],
        updateConfigs: [],
        commit: false,
        commitMessage: 'Release v%VERSION%',
        commitFiles: [
          'package.json',
          'bower.json'
        ],
        createTag: true,
        tagName: 'v%VERSION%',
        tagMessage: 'Version %VERSION%',
        push: false,
        pushTo: 'upstream',
        gitDescribeOptions: '--tags --always --abbrev=1 --dirty=-d',
        globalReplace: false,
        prereleaseName: false,
        regExp: false
      }
    }


    // By default, your `index.html`'s <!-- Usemin block --> will take care of
    // minification. These next options are pre-configured if you do not wish
    // to use the Usemin blocks.
    // cssmin: {
    //   dist: {
    //     files: {
    //       '<%= yeoman.dist %>/<%= yeoman.styles %>/main.css': [
    //         '.temp/<%= yeoman.styles %>/**/*.css',
    //         '<%= yeoman.app %>/<%= yeoman.styles %>/**/*.css'
    //       ]
    //     }
    //   }
    // },
    // uglify: {
    //   dist: {
    //     files: {
    //       '<%= yeoman.dist %>/<%= yeoman.scripts %>/scripts.js': [
    //         '<%= yeoman.dist %>/<%= yeoman.scripts %>/scripts.js'
    //       ]
    //     }
    //   }
    // },
    // concat: {
    //   dist: {}
    // },

    // Test settings
    // These will override any config options in karma.conf.js if you create it.
    // karma: {
    //   options: {
    //     basePath: '',
    //     frameworks: ['mocha', 'chai'],
    //     files: [
    //       '<%= yeoman.app %>/bower_components/angular/angular.js',
    //       '<%= yeoman.app %>/bower_components/angular-mocks/angular-mocks.js',
    //       '<%= yeoman.app %>/bower_components/angular-animate/angular-animate.js',
    //       '<%= yeoman.app %>/bower_components/angular-sanitize/angular-sanitize.js',
    //       '<%= yeoman.app %>/bower_components/angular-ui-router/release/angular-ui-router.js',
    //       '<%= yeoman.app %>/bower_components/ionic/release/js/ionic.js',
    //       '<%= yeoman.app %>/bower_components/ionic/release/js/ionic-angular.js',
    //       '<%= yeoman.app %>/<%= yeoman.scripts %>/**/*.js',
    //       '<%= yeoman.test %>/mock/**/*.js',
    //       '<%= yeoman.test %>/spec/**/*.js'
    //     ],
    //     autoWatch: false,
    //     reporters: ['dots', 'coverage'],
    //     port: 8080,
    //     singleRun: false,
    //     preprocessors: {
    //       // Update this if you change the yeoman config path
    //       '<%= yeoman.app %>/<%= yeoman.scripts %>/**/*.js': ['coverage']
    //     },
    //     coverageReporter: {
    //       reporters: [{
    //         type: 'html',
    //         dir: 'coverage/'
    //       }, {
    //         type: 'text-summary'
    //       }]
    //     }
    //   },
    //   unit: {
    //     // Change this to 'Chrome', 'Firefox', etc. Note that you will need
    //     // to install a karma launcher plugin for browsers other than Chrome.
    //     browsers: ['PhantomJS'],
    //     background: true
    //   },
    //   continuous: {
    //     browsers: ['PhantomJS'],
    //     singleRun: true
    //   }
    // },



  });

  // Register tasks for all Cordova commands
  // _.functions(cordovaCli).forEach(function(name) {
  //   grunt.registerTask(name, function() {
  //     this.args.unshift(name.replace('cordova:', ''));
  //     // Handle URL's being split up by Grunt because of `:` characters
  //     if (_.contains(this.args, 'http') || _.contains(this.args, 'https')) {
  //       this.args = this.args.slice(0, -2).concat(_.last(this.args, 2).join(':'));
  //     }
  //     var done = this.async();
  //     var exec = process.platform === 'win32' ? 'cordova.cmd' : 'cordova';
  //     var cmd = path.resolve('./node_modules/cordova/bin', exec);
  //     var flags = process.argv.splice(3);
  //     var child = spawn(cmd, this.args.concat(flags));
  //     child.stdout.on('data', function(data) {
  //       grunt.log.writeln(data);
  //     });
  //     child.stderr.on('data', function(data) {
  //       grunt.log.error(data);
  //     });
  //     child.on('close', function(code) {
  //       code = code ? false : true;
  //       done(code);
  //     });
  //   });
  // });

  // Since Apache Ripple serves assets directly out of their respective platform
  // directories, we watch all registered files and then copy all un-built assets
  // over to <%= yeoman.dist %>/. Last step is running cordova prepare so we can refresh the ripple
  // browser tab to see the changes. Technically ripple runs `cordova prepare` on browser
  // refreshes, but at this time you would need to re-run the emulator to see changes.
  // grunt.registerTask('ripple', ['wiredep', 'newer:copy:app', 'ripple-emulator']);
  // grunt.registerTask('ripple-emulator', function() {
  //   grunt.config.set('watch', {
  //     all: {
  //       files: _.flatten(_.pluck(grunt.config.get('watch'), 'files')),
  //       tasks: ['newer:copy:app', 'prepare']
  //     }
  //   });
  //
  //   var cmd = path.resolve('./node_modules/ripple-emulator/bin', 'ripple');
  //   var child = spawn(cmd, ['emulate']);
  //   child.stdout.on('data', function(data) {
  //     grunt.log.writeln(data);
  //   });
  //   child.stderr.on('data', function(data) {
  //     grunt.log.error(data);
  //   });
  //   process.on('exit', function(code) {
  //     child.kill('SIGINT');
  //     process.exit(code);
  //   });
  //
  //   return grunt.task.run(['watch']);
  // });

  // Dynamically configure `karma` target of `watch` task so that
  // we don't have to run the karma test server as part of `grunt serve`
  // grunt.registerTask('watch:karma', function() {
  //   var karma = {
  //     files: ['<%= yeoman.app %>/<%= yeoman.scripts %>/**/*.js', '<%= yeoman.test %>/spec/**/*.js'],
  //     tasks: ['newer:jshint:test', 'karma:unit:run']
  //   };
  //   grunt.config.set('watch', karma);
  //   return grunt.task.run(['watch']);
  // });

  // Wrap ionic-cli commands
  grunt.registerTask('ionic', function() {
    var done = this.async();
    var script = path.resolve('./node_modules/ionic/bin/', 'ionic');
    var flags = process.argv.splice(3);
    var child = spawn(script, this.args.concat(flags), {
      stdio: 'inherit'
    });
    child.on('close', function(code) {
      code = code ? false : true;
      done(code);
    });
  });

  grunt.registerTask('test', [
    'wiredep',
    'clean',
    'concurrent:test',
    'autoprefixer',
    'karma:unit:start',
    'watch:karma'
  ]);


/*
  grunt.registerTask('serve', function(target) {
    if (target === 'compress') {
      return grunt.task.run(['compress', 'ionic:serve']);
    }

    grunt.config('concurrent.ionic.tasks', ['ionic:serve', 'watch']);
    grunt.task.run(['wiredep', 'init', 'concurrent:ionic']);
  });
*/

  grunt.registerTask('serve', 'Compile then start a connect web server', function (target) {

  if (target === 'dist') {
    return grunt.task.run(['build', 'connect:dist:keepalive']);
  }

  grunt.config('concurrent.ionic.tasks', [
      'ionic:serve',
      'watch'
  ]);

  grunt.task.run([
    'clean:dist',
    'wiredep',
    'compass',
    'useminPrepare',
    'copy:dist',
    'autoprefixer:server',
    'connect:livereload',
    'watch'
  ]);
});

  grunt.registerTask('emulate', function(target, buildType) {
    grunt.config('concurrent.ionic.tasks', ['ionic:emulate:' + this.args.join()]);//, 'watch'
    return grunt.task.run(['init', 'concurrent:ionic',  'autoprefixer']);
  });

  grunt.registerTask('run', function(target, buildType) {
    grunt.config('concurrent.ionic.tasks', ['ionic:run:' + this.args.join(), 'watch']);
    return grunt.task.run(['init', 'concurrent:ionic', 'autoprefixer']); //'autoprefixer',
  });

  grunt.registerTask('build', function (target, buildType) {
      grunt.config('concurrent.ionic.tasks', [
          'ionic:build:' + this.args.join()
      ]);
      return grunt.task.run([
        'init',
        'concurrent:ionic',
        'autoprefixer'
      ]);

  });

  grunt.registerTask('default', [
   'newer:jshint',
   'test',
   'build'
 ]);

  grunt.registerTask('init', [
      'clean:dist',
      'wiredep',
      'compass',
      'useminPrepare',
      'concat',
      'ngAnnotate',
      'copy:dist',
      //'autoprefixer',
      'uglify',
      'usemin'
  ]);




/*
  grunt.registerTask('compress', [
    'clean',
    'wiredep',
    'useminPrepare',
    'concurrent:dist',
    'autoprefixer',
    'concat',
    'ngAnnotate',
    'copy:dist',
    'cssmin',
    'uglify',
    'usemin',
    'htmlmin'
  ]);
*/

};
