/**
 * @ngdoc service
 * @name guest.guest
 * @description
 * # guest
 * `guest` is a factory in the `guest` module.
 */
(function(module) {
  'use strict';

  module.factory('EventDetail', function($http, $q, CONFIG) {

    return {

      /**
       * Get google place list within n mile from passed geo location
       * @param : _lat(float) |  _lng(float)  | _type(string) : ex: hospital, restaurant
       * @returns : list of place (20 items) object
       * example url: https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=38.899227,-77.027969&radius=48300&types=hospital&key=AIzaSyA1dZV49G9hkeacYcnw92afPqA6RRqaSeM
       * if you want to sort by distance : rankby=distance
       * list of types : https://developers.google.com/places/supported_types#table1
       */



    }

  });
})(angular.module('mbbc.eventDetail'));
