// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
'use strict';

angular.module('mbbc')

.run(function($ionicPlatform, $state) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    //function init() {
    //$rootScope.$state = $state;
    //}
    //init();

  });
})

.constant("CONFIG", {
    appName: "MBBC",
    appVersion: "1.0.0",
    basedURL : "https://api.vimeo.com/"
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
  .state('layout', {
    abstract: true,
    templateUrl: 'scripts/layout.html',
    controller : 'appCtrl'
  })

  .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'scripts/tabs/tabs.html',
    controller : 'tabCtrl'
  })

  // Each tab has its own nav history stack:
  .state('tab.sermons', {
      url: '/sermons',
      views: {
        'tab-sermons': {
          templateUrl: 'scripts/sermons/sermons.html',
          controller: 'sermonCtrl'
        }
      }
    })
    .state('tab.events', {
      url: '/events',
      views: {
        'tab-events': {
          templateUrl: 'scripts/events/events.html',
          controller: 'eventsCtrl'
        }
      }
    })
    .state('tab.churchInfo', {
      url: '/churchInfo',
      views: {
        'tab-churchInfo': {
          templateUrl: 'scripts/churchInfo/churchInfo.html',
          controller: 'churchInfoCtrl'
        }
      }
    })
    .state('tab.aboutUs', {
      url: '/aboutUs',
      views: {
        'tab-aboutUs': {
          templateUrl: 'scripts/aboutUs/aboutUs.html',
          controller: 'aboutUsCtrl'
        }
      }
    })
    .state('tab.sermonDetail', {
        url: '/sermonDetail/:item',
        params:{
          item:null
        },
        views: {
          'tab-sermons': {
            templateUrl: 'scripts/sermonDetail/sermonDetail.html',
            controller: 'sermonDetailCtrl'
          }
        }
      })
       .state('tab.eventDetail', {
        url: '/eventDetail/:item',
        params:{
          item:null
        },
        views: {
          'tab-events': {
            templateUrl: 'scripts/eventDetail/eventDetail.html',
            controller: 'eventDetailCtrl'
          }
        }
      })

      .state('bible', {
       url: '/bible',
       parent: 'layout',
       views: {
         'menu': {
           templateUrl: 'scripts/bible/books.html',
           controller: 'bibleCtrl',
         }
       }

     })
     .state('chapters', {
      url: '/chapters',
      parent: 'layout',
      views: {
        'menu': {
          templateUrl: 'scripts/bible/chapters.html',
          controller: 'bibleCtrl',
        }
      }
    });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/sermons');

})
.controller('appCtrl', function ($scope, $state, $ionicHistory){
  $scope.goBack = function(){
    $ionicHistory.goBack();
  }
})
.directive('focusMe', function($timeout) {
  return {
    link: function(scope, element, attrs) {
      $timeout(function() {
        element[0].focus();
        if (ionic.Platform.isAndroid()) {
          cordova.plugins.Keyboard.show();
        }
      }, 150);
    }
  };
});
