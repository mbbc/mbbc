/**
 * @ngdoc function
 * @name guest.GuestController
 * @description
 * # GuestController
 * `GuestController` is a controller in the `guest` module.
 */
(function (module) {

  'use strict';

   module.controller('tabCtrl', function ($scope, $state, $ionicHistory) {

     /* open view */
     $scope.openView = function(viewName){
       $state.go(viewName, {});
     }

     $scope.goBack = function(){
       $ionicHistory.goBack();
     }

  });
})(angular.module('mbbc.tab'));
