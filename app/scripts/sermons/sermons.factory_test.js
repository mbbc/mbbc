//how to test plugins
describe('Factory Sermons', function() {

  // load the service's module
  beforeEach(module('mbbc.sermons'));

var badgeprintFactory, $q, $window, rootScope, $ionicPlatform, httpBackend,
      deferred;

  beforeEach(inject(function(_BadgeprintFactory_, _$q_, $rootScope, $httpBackend, _$ionicPlatform_, _$window_) {
    badgeprintFactory = _BadgeprintFactory_;
    $q = _$q_
    rootScope = $rootScope;
    httpBackend = $httpBackend;
    $ionicPlatform = _$ionicPlatform_;
    $window = _$window_;
    
    $window.cordova = {
      plugins: {
        printer: {
          isAvailable: jasmine.createSpy('isAvailable'),
          print      : jasmine.createSpy('print')
        }
      }
    };

    deferred = $q.defer();

  }));

it('isAvailable have been called', function() {
    spyOn(badgeprintFactory, 'isAvailable').and.callThrough();
    $ionicPlatform.ready(function() {
      badgeprintFactory.isAvailable();
      $window.cordova.plugins.printer.isAvailable();
      deferred.resolve();
      expect(badgeprintFactory.isAvailable).toHaveBeenCalled();
  });
  });
});
