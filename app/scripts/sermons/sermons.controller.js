/**
 * @ngdoc function
 * @name guest.GuestController
 * @description
 * # GuestController
 * `GuestController` is a controller in the `guest` module.
 */
(function (module) {

  'use strict';

   module.controller('sermonCtrl', function ($scope, $q, $state, Sermons) {




$scope.sermonList = [];
$scope.page = 0;
$scope.totalPages = 10;
$scope.noMoreItemsAvailable = false;

       $scope.getSermons = function(loadmore){

         Sermons.getSermons({
           page: $scope.page
         }).then(function(res){

           $scope.$broadcast('scroll.refreshComplete');
           $scope.$broadcast('scroll.infiniteScrollComplete');

           $scope.totalPages = parseInt(Math.ceil(res.total / 10));

           if(loadmore){
             $scope.sermonList = $scope.sermonList.concat(res.data);
           }else{
             $scope.sermonList = res.data;
           }
        //   console.log(res.data);

         }, function(err){
           $scope.$broadcast('scroll.refreshComplete');
           $scope.$broadcast('scroll.infiniteScrollComplete');
           console.log(err);
         });

       }

       /*
       * loadmore when user scroll up, it will get next page
       */
       $scope.loadmore = function(){
         $scope.page = $scope.page + 1;

         if($scope.totalPages >= $scope.page){
            $scope.getSermons(true);
         }else{
           $scope.noMoreItemsAvailable = true;
         }
       }


       /*
       * refresh data
       */
        $scope.doRefresh = function(){
          $scope.page = 1;
          $scope.noMoreItemsAvailable = false;
          $scope.sermonList = []; //clear
          $scope.getSermons();
        }

        $scope.openView = function(index){

          $state.go("tab.sermonDetail",

                    {item:JSON.stringify($scope.sermonList[index])}

                  );

        }
//$state.param

  });
})(angular.module('mbbc.sermons'));
