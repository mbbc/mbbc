/**
 * @ngdoc function
 * @name guest.GuestController
 * @description
 * # GuestController
 * `GuestController` is a controller in the `guest` module.
 */
(function (module) {

  'use strict';

   module.controller('eventsCtrl', function ($scope, $q, $state) {

         $scope.eventList = [

         {"img" : "images/ionic.png", "title": "event title" , "date" : "show date here"}
        ,{"img" : "images/ionic.png", "title": "event title" , "date" : "show date here"}
        ,{"img" : "images/ionic.png", "title": "event title" , "date" : "show date here"}
        ,{"img" : "images/ionic.png", "title": "event title" , "date" : "show date here"}
        ,{"img" : "images/ionic.png", "title": "event title" , "date" : "show date here"}
        ,{"img" : "images/ionic.png", "title": "event title" , "date" : "show date here"}
        ,{"img" : "images/ionic.png", "title": "event title" , "date" : "show date here"}
        ,{"img" : "images/ionic.png", "title": "event title" , "date" : "show date here"}
        ,{"img" : "images/ionic.png", "title": "event title" , "date" : "show date here"}
        ,{"img" : "images/ionic.png", "title": "event title" , "date" : "show date here"}
        ,{"img" : "images/ionic.png", "title": "event title" , "date" : "show date here"}
        ,{"img" : "images/ionic.png", "title": "event title" , "date" : "show date here"}
        ,{"img" : "images/ionic.png", "title": "event title" , "date" : "show date here"}
        ,{"img" : "images/ionic.png", "title": "event title" , "date" : "show date here"}
        ,{"img" : "images/ionic.png", "title": "event title" , "date" : "show date here"}
        ,{"img" : "images/ionic.png", "title": "event title" , "date" : "show date here"}
        ,{"img" : "images/ionic.png", "title": "event title" , "date" : "show date here"}
        ,{"img" : "images/ionic.png", "title": "event title" , "date" : "show date here"}
     ];

       $scope.openView = function(){

          $state.go("tab.eventDetail",

                    {item:"abc"}

                  );

        }

  });
})(angular.module('mbbc.events'));
