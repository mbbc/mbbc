/**
 * @ngdoc module
 * @name guest
 * @overview
 * # guest
 * `guest` is a module dedicated to doing really cool stuff.
 */
(function (angular) {
    'use strict';

    angular.module('mbbc.events', [
      //'ui.router',
      //'vzcheckin.components.guest.summary',
      //'vzcheckin.components.host',
      //'vzcheckin.shared.camera',
      //'vzcheckin.shared.datehelper',
      //'ngLodash',
        // add dependencies to the dependencies array
    ]);
})(angular);
