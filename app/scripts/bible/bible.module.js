/**
 * @ngdoc module
 * @name guest
 * @overview
 * # guest
 * `guest` is a module dedicated to doing really cool stuff.
 */
(function (angular) {
    'use strict';

    angular.module('mbbc.bible', [
      
    ]);
})(angular);
