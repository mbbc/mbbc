/**
 * @ngdoc service
 * @name guest.guest
 * @description
 * # guest
 * `guest` is a factory in the `guest` module.
 */
(function (module) {
    'use strict';

    module.service('BibleService', function ($http, $q) {

      var books = [{
      	BIBLE_BOOK_ID : 1,
      	ENG : 'Genesis',
      	KOR : '창세기',
      	ESP : 'Genesis',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 50
      }, {
      	BIBLE_BOOK_ID : 2,
      	ENG : 'Exodus',
      	KOR : '출애굽기',
      	ESP : 'Exodo',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 40
      }, {
      	BIBLE_BOOK_ID : 3,
      	ENG : 'Leviticus',
      	KOR : '레위기',
      	ESP : 'Levitico',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 27
      }, {
      	BIBLE_BOOK_ID : 4,
      	ENG : 'Numbers',
      	KOR : '민수기',
      	ESP : 'Numeros',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 36
      }, {
      	BIBLE_BOOK_ID : 5,
      	ENG : 'Deuteronomy',
      	KOR : '신명기',
      	ESP : 'Deuteronomio',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 34
      }, {
      	BIBLE_BOOK_ID : 6,
      	ENG : 'Joshua',
      	KOR : '여호수아',
      	ESP : 'Josue',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 24
      }, {
      	BIBLE_BOOK_ID : 7,
      	ENG : 'Judges',
      	KOR : '사사기',
      	ESP : 'Jueces',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 21
      }, {
      	BIBLE_BOOK_ID : 8,
      	ENG : 'Ruth',
      	KOR : '룻기',
      	ESP : 'Rut',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 4
      }, {
      	BIBLE_BOOK_ID : 9,
      	ENG : '1 Samuel',
      	KOR : '사무엘상',
      	ESP : '1 Samuel',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 31
      }, {
      	BIBLE_BOOK_ID : 10,
      	ENG : '2 Samuel',
      	KOR : '사무엘하',
      	ESP : '2 Samuel',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 24
      }, {
      	BIBLE_BOOK_ID : 11,
      	ENG : '1 Kings',
      	KOR : '열왕기상',
      	ESP : '1 Reyes',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 22
      }, {
      	BIBLE_BOOK_ID : 12,
      	ENG : '2 Kings',
      	KOR : '열왕기하',
      	ESP : '2 Reyes',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 25
      }, {
      	BIBLE_BOOK_ID : 13,
      	ENG : '1 Chronicles',
      	KOR : '역대기상',
      	ESP : '1 Cronicas',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 29
      }, {
      	BIBLE_BOOK_ID : 14,
      	ENG : '2 Chronicles',
      	KOR : '역대기하',
      	ESP : '2 Cronicas',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 36
      }, {
      	BIBLE_BOOK_ID : 15,
      	ENG : 'Ezra',
      	KOR : '에스라',
      	ESP : 'Esdras',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 10
      }, {
      	BIBLE_BOOK_ID : 16,
      	ENG : 'Nehemiah',
      	KOR : '느헤미야',
      	ESP : 'Nehemias',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 13
      }, {
      	BIBLE_BOOK_ID : 17,
      	ENG : 'Esther',
      	KOR : '에스더',
      	ESP : 'Ester',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 10
      }, {
      	BIBLE_BOOK_ID : 18,
      	ENG : 'Job',
      	KOR : '욥기',
      	ESP : 'Job',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 42
      }, {
      	BIBLE_BOOK_ID : 19,
      	ENG : 'Psalms',
      	KOR : '시편',
      	ESP : 'Salmos',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 150
      }, {
      	BIBLE_BOOK_ID : 20,
      	ENG : 'Proverbs',
      	KOR : '잠언',
      	ESP : 'Proverbios',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 31
      }, {
      	BIBLE_BOOK_ID : 21,
      	ENG : 'Ecclesiastes',
      	KOR : '전도서',
      	ESP : 'Eclesiastes',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 12
      }, {
      	BIBLE_BOOK_ID : 22,
      	ENG : 'Song of Solomon',
      	KOR : '아가',
      	ESP : 'Cantares',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 8
      }, {
      	BIBLE_BOOK_ID : 23,
      	ENG : 'Isaiah',
      	KOR : '이사야',
      	ESP : 'Isaias',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 66
      }, {
      	BIBLE_BOOK_ID : 24,
      	ENG : 'Jeremiah',
      	KOR : '예레미야',
      	ESP : 'Jeremias',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 52
      }, {
      	BIBLE_BOOK_ID : 25,
      	ENG : 'Lamentations',
      	KOR : '예레미야애가',
      	ESP : 'Lamentaciones',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 5
      }, {
      	BIBLE_BOOK_ID : 26,
      	ENG : 'Ezekiel',
      	KOR : '에스겔',
      	ESP : 'Ezequiel',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 48
      }, {
      	BIBLE_BOOK_ID : 27,
      	ENG : 'Daniel',
      	KOR : '다니엘',
      	ESP : 'Daniel',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 12
      }, {
      	BIBLE_BOOK_ID : 28,
      	ENG : 'Hosea',
      	KOR : '호세아',
      	ESP : 'Oseas',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 14
      }, {
      	BIBLE_BOOK_ID : 29,
      	ENG : 'Joel',
      	KOR : '요엘',
      	ESP : 'Joel',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 3
      }, {
      	BIBLE_BOOK_ID : 30,
      	ENG : 'Amos',
      	KOR : '아모스',
      	ESP : 'Amos',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 9
      }, {
      	BIBLE_BOOK_ID : 31,
      	ENG : 'Obadiah',
      	KOR : '오바댜',
      	ESP : 'Abdias',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 1
      }, {
      	BIBLE_BOOK_ID : 32,
      	ENG : 'Jonah',
      	KOR : '요나',
      	ESP : 'Jonas',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 4
      }, {
      	BIBLE_BOOK_ID : 33,
      	ENG : 'Micah',
      	KOR : '미가',
      	ESP : 'Miqueas',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 7
      }, {
      	BIBLE_BOOK_ID : 34,
      	ENG : 'Nahum',
      	KOR : '나훔',
      	ESP : 'Nahum',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 3
      }, {
      	BIBLE_BOOK_ID : 35,
      	ENG : 'Habakkuk',
      	KOR : '하박국',
      	ESP : 'Habacuc',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 3
      }, {
      	BIBLE_BOOK_ID : 36,
      	ENG : 'Zephaniah',
      	KOR : '스바냐',
      	ESP : 'Sofonias',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 3
      }, {
      	BIBLE_BOOK_ID : 37,
      	ENG : 'Haggai',
      	KOR : '학개',
      	ESP : 'Hageo',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 2
      }, {
      	BIBLE_BOOK_ID : 38,
      	ENG : 'Zechariah',
      	KOR : '스가랴',
      	ESP : 'Zacarias',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 14
      }, {
      	BIBLE_BOOK_ID : 39,
      	ENG : 'Malachi',
      	KOR : '말라기',
      	ESP : 'Malaquias',
      	OLD_NEW : 0,
      	COUNT_CHAPTER : 4
      }, {
      	BIBLE_BOOK_ID : 40,
      	ENG : 'Matthew',
      	KOR : '마태복음',
      	ESP : 'Mateo',
      	OLD_NEW : 1,
      	COUNT_CHAPTER : 28
      }, {
      	BIBLE_BOOK_ID : 41,
      	ENG : 'Mark',
      	KOR : '마가복음',
      	ESP : 'Marcos',
      	OLD_NEW : 1,
      	COUNT_CHAPTER : 16
      }, {
      	BIBLE_BOOK_ID : 42,
      	ENG : 'Luke',
      	KOR : '누가복음',
      	ESP : 'Lucas',
      	OLD_NEW : 1,
      	COUNT_CHAPTER : 24
      }, {
      	BIBLE_BOOK_ID : 43,
      	ENG : 'John',
      	KOR : '요한복음',
      	ESP : 'Juan',
      	OLD_NEW : 1,
      	COUNT_CHAPTER : 21
      }, {
      	BIBLE_BOOK_ID : 44,
      	ENG : 'Acts',
      	KOR : '사도행전',
      	ESP : 'Hechos',
      	OLD_NEW : 1,
      	COUNT_CHAPTER : 28
      }, {
      	BIBLE_BOOK_ID : 45,
      	ENG : 'Romans',
      	KOR : '로마서',
      	ESP : 'Romanos',
      	OLD_NEW : 1,
      	COUNT_CHAPTER : 16
      }, {
      	BIBLE_BOOK_ID : 46,
      	ENG : '1 Corinthians',
      	KOR : '고린도전서',
      	ESP : '1 Corintios',
      	OLD_NEW : 1,
      	COUNT_CHAPTER : 16
      }, {
      	BIBLE_BOOK_ID : 47,
      	ENG : '2 Corinthians',
      	KOR : '고린도후서',
      	ESP : '2 Corintios',
      	OLD_NEW : 1,
      	COUNT_CHAPTER : 13
      }, {
      	BIBLE_BOOK_ID : 48,
      	ENG : 'Galatians',
      	KOR : '갈라디아서',
      	ESP : 'Galatas',
      	OLD_NEW : 1,
      	COUNT_CHAPTER : 6
      }, {
      	BIBLE_BOOK_ID : 49,
      	ENG : 'Ephesians',
      	KOR : '에베소서',
      	ESP : 'Efesios',
      	OLD_NEW : 1,
      	COUNT_CHAPTER : 6
      }, {
      	BIBLE_BOOK_ID : 50,
      	ENG : 'Philippians',
      	KOR : '빌립보서',
      	ESP : 'Filipenses',
      	OLD_NEW : 1,
      	COUNT_CHAPTER : 4
      }, {
      	BIBLE_BOOK_ID : 51,
      	ENG : 'Colossians',
      	KOR : '골로새서',
      	ESP : 'Colosenses',
      	OLD_NEW : 1,
      	COUNT_CHAPTER : 4
      }, {
      	BIBLE_BOOK_ID : 52,
      	ENG : '1 Thessalonians',
      	KOR : '데살로니가전서',
      	ESP : '1 Tesalonicenses',
      	OLD_NEW : 1,
      	COUNT_CHAPTER : 5
      }, {
      	BIBLE_BOOK_ID : 53,
      	ENG : '2 Thessalonians',
      	KOR : '데살로니가후서',
      	ESP : '2 Tesalonicenses',
      	OLD_NEW : 1,
      	COUNT_CHAPTER : 3
      }, {
      	BIBLE_BOOK_ID : 54,
      	ENG : '1 Timothy',
      	KOR : '디모데전서',
      	ESP : '1 Timoteo',
      	OLD_NEW : 1,
      	COUNT_CHAPTER : 6
      }, {
      	BIBLE_BOOK_ID : 55,
      	ENG : '2 Timothy',
      	KOR : '디모데후서',
      	ESP : '2 Timoteo',
      	OLD_NEW : 1,
      	COUNT_CHAPTER : 4
      }, {
      	BIBLE_BOOK_ID : 56,
      	ENG : 'Titus',
      	KOR : '디도서',
      	ESP : 'Tito',
      	OLD_NEW : 1,
      	COUNT_CHAPTER : 3
      }, {
      	BIBLE_BOOK_ID : 57,
      	ENG : 'Philemon',
      	KOR : '빌레몬서',
      	ESP : 'Filemon',
      	OLD_NEW : 1,
      	COUNT_CHAPTER : 1
      }, {
      	BIBLE_BOOK_ID : 58,
      	ENG : 'Hebrews',
      	KOR : '히브리서',
      	ESP : 'Hebreos',
      	OLD_NEW : 1,
      	COUNT_CHAPTER : 13
      }, {
      	BIBLE_BOOK_ID : 59,
      	ENG : 'James',
      	KOR : '야고보서',
      	ESP : 'Santiago',
      	OLD_NEW : 1,
      	COUNT_CHAPTER : 5
      }, {
      	BIBLE_BOOK_ID : 60,
      	ENG : '1 Peter',
      	KOR : '베드로전서',
      	ESP : '1 Pedro',
      	OLD_NEW : 1,
      	COUNT_CHAPTER : 5
      }, {
      	BIBLE_BOOK_ID : 61,
      	ENG : '2 Peter',
      	KOR : '베드로후서',
      	ESP : '2 Pedro',
      	OLD_NEW : 1,
      	COUNT_CHAPTER : 3
      }, {
      	BIBLE_BOOK_ID : 62,
      	ENG : '1 John',
      	KOR : '요한1서',
      	ESP : '1 Juan',
      	OLD_NEW : 1,
      	COUNT_CHAPTER : 5
      }, {
      	BIBLE_BOOK_ID : 63,
      	ENG : '2 John',
      	KOR : '요한2서',
      	ESP : '2 Juan',
      	OLD_NEW : 1,
      	COUNT_CHAPTER : 1
      }, {
      	BIBLE_BOOK_ID : 64,
      	ENG : '3 John',
      	KOR : '요한3서',
      	ESP : '3 Juan',
      	OLD_NEW : 1,
      	COUNT_CHAPTER : 1
      }, {
      	BIBLE_BOOK_ID : 65,
      	ENG : 'Jude',
      	KOR : '유다서',
      	ESP : 'Judas',
      	OLD_NEW : 1,
      	COUNT_CHAPTER : 1
      }, {
      	BIBLE_BOOK_ID : 66,
      	ENG : 'Revelation',
      	KOR : '요한계시록',
      	ESP : 'Apocalipsis',
      	OLD_NEW : 1,
      	COUNT_CHAPTER : 22
      }];

      var basedBibleURL = "http://labs.bible.org/api/?passage=";// + "John+3" + "&type=json";

      return {
        getBooks: function(){
          return books;
        },

        getPassage: function(params){
          var url = basedBibleURL + params.book + "+" + params.chapter + "+" + params.chapterCount + "&type=json";

        }


      }
  });

})(angular.module('mbbc.bible'));
