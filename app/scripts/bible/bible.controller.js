/**
 * @ngdoc function
 * @name guest.GuestController
 * @description
 * # GuestController
 * `GuestController` is a controller in the `guest` module.
 */
(function (module) {

  'use strict';

   module.controller('bibleCtrl', function ($scope, $q, $state, BibleService, $ionicHistory) {

     $scope.books = BibleService.getBooks();

     $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
        viewData.enableBack = true;
     }); 

     $scope.goBack = function(){
       $ionicHistory.goBack();
     }

     $scope.openChapter = function(){
       $state.go("chapters");
     }


  });
})(angular.module('mbbc.bible'));
