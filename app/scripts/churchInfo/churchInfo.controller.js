/**
 * @ngdoc function
 * @name guest.GuestController
 * @description
 * # GuestController
 * `GuestController` is a controller in the `guest` module.
 */
(function (module) {

  'use strict';

   module.controller('churchInfoCtrl', function ($scope, $q, $state, $compile, $ionicPlatform) {

     $scope.emailData = {
      name: ""
     ,email: ""
     ,phone:  ""
     ,message: ""
     };

    $scope.emailFunction = function (){

console.log($scope.emailData);

     };

     $ionicPlatform.ready(function(){
       initialize();
     });

     function initialize() {
        var churchPosition = new google.maps.LatLng(32.9194428,-96.9554905);

        var mapOptions = {
          center: churchPosition,
          zoom: 16,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map"), mapOptions);

        //Marker + infowindow + angularjs compiled ng-click
        var contentString = "<div><a ng-click='openNavigation()'>MBBC</a></div>";
        var compiled = $compile(contentString)($scope);

        var infowindow = new google.maps.InfoWindow({
          content: compiled[0]
        });

        var marker = new google.maps.Marker({
          position: churchPosition,
          map: map,
          title: 'Uluru (Ayers Rock)'
        });

        google.maps.event.addListener(marker, 'click', function() {
          infowindow.open(map,marker);
        });

        $scope.map = map;
      }

      function getCurrentLocation(){
        navigator.geolocation.getCurrentPosition(function(pos) {
          $scope.map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
        }, function(error) {
          alert('Unable to get location: ' + error.message);
        });
      }
      

      $scope.openNavigation = function(){
        console.log("fired");
      }








  });
})(angular.module('mbbc.churchInfo'));
