/**
 * @ngdoc module
 * @name guest
 * @overview
 * # guest
 * `guest` is a module dedicated to doing really cool stuff.
 */
(function (angular) {
    'use strict';

    angular.module('mbbc', [
      'ui.router',
      'ionic',
      'mbbc.tab',
      'mbbc.sermons',
      'mbbc.events',
      'mbbc.churchInfo',
      'mbbc.aboutUs',
      'mbbc.common',
      'mbbc.sermonDetail',
      'mbbc.eventDetail',
      'mbbc.bible',
      'ngAnimate'
    ]);
})(angular);
