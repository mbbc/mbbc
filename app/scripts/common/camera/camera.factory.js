/*
cameraService helps handling all camera works.
requirement to do :

cordova plugin add cordova-plugin-camera
cordova plugin add https://github.com/timkalinowski/PhoneGap-Image-Resizer
cordova plugin add cordova-plugin-media-capture
cordova plugin add https://github.com/wymsee/cordova-imagePicker.git
cordova plugin add https://github.com/EddyVerbruggen/cordova-plugin-actionsheet
*/
(function(module) {
  'use strict';

  module.factory('Camera', function($q, $http, $ionicLoading, $ionicPopup, $log) {//$cordovaCapture, $cordovaImagePicker
    return {

      /**
       * Multiple Image Selection - implemented for iOS and Android 4.0 and above.
       * @returns photo object
       */
      getPicturesFromGallery: function() {
        var q = $q.defer();

        var options = {
          maximumImagesCount: 1,
          width: 800,
          height: 800,
          quality: 80,
        };

        $cordovaImagePicker.getPictures(options).then(function(results) {
          q.resolve(results[0]);
        }, function(error) {
          q.reject(error);
        });

        return q.promise;
      },


      /**
       * Open user's device photo
       *
       * @returns photo object
       */
      getPicture: function(options) {
        var q = $q.defer();

        navigator.camera.getPicture(function(result) {
          // Do any magic you need
          q.resolve(result);
        }, function(err) {
          q.reject(err);
        }, options);

        return q.promise;
      },


      /**
       * Image resizer function : If image quality is too high(ex:iphone6plus), app crashes. So we need to resize image before upload or pass.
       * @param _imgPath (string)
       * @returns image object
       * @require : cordova plugin add https://github.com/timkalinowski/PhoneGap-Image-Resizer
       */
      resizeImage: function(_imgPath) {
        var q = $q.defer();
        window.imageResizer.resizeImage(function(success_resp) {
          $log.debug('success, img re-size: ' + JSON.stringify(success_resp));
          q.resolve(success_resp);
        }, function(fail_resp) {
          $log.debug('fail, img re-size: ' + JSON.stringify(fail_resp));
          q.reject(fail_resp);
        }, _imgPath, 200, 0, {
          imageDataType: ImageResizer.IMAGE_DATA_TYPE_URL,
          resizeType: ImageResizer.RESIZE_TYPE_MIN_PIXEL,
          pixelDensity: true,
          storeImage: false,
          photoAlbum: false,
          format: 'jpg'
        });

        return q.promise;
      },

      /**
       * Make image to base64 for uploading process
       * @param _imgPath(string)
       * @returns photo object
       */
      toBase64Image: function(_imgPath) {
        var q = $q.defer();
        window.imageResizer.resizeImage(function(success_resp) {
          $log.debug('success, img toBase64Image: ' + JSON.stringify(success_resp));
          q.resolve(success_resp);
        }, function(fail_resp) {
          $log.debug('fail, img toBase64Image: ' + JSON.stringify(fail_resp));
          q.reject(fail_resp);
        }, _imgPath, 1, 1, {
          imageDataType: ImageResizer.IMAGE_DATA_TYPE_URL,
          resizeType: ImageResizer.RESIZE_TYPE_FACTOR,
          format: 'jpg'
        });

        return q.promise;
      },

      /**
       * displays the photo gallery for the user to select an image to
       * work with
       */
      doGetGalleryPhoto: function doGetGalleryPhoto() {
        return this.getPicturesFromGallery();
      },

      /**
       * displays the camera for the user to select/take a photo
       */
      doGetProfilePhoto: function doGetProfilePhoto() {
        var picOptions = {
          destinationType: navigator.camera.DestinationType.FILE_URI,
          quality: 75,
          targetWidth: 500,
          targetHeight: 500,
          allowEdit: true,
          saveToPhotoAlbum: false
        };

        return this.getPicture(picOptions)
      },

      /**
       * displays the camera for the user to select/take a photo
       */
      doGetVideo: function doGetVideo() {
        var q = $q.defer();

        //video options
        var options = {
          destinationType: navigator.camera.DestinationType.FILE_URI,
          mediaType: navigator.camera.MediaType.VIDEO,
          sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
          quality: 50,
          targetWidth: 500,
          targetHeight: 500,
          //allowEdit: true,
          saveToPhotoAlbum: false
        };

        navigator.camera.getPicture(function(result) {
          // Do any magic you need
          q.resolve(result);
        }, function(err) {
          q.reject(err);
        }, options);

        return q.promise;

      },

      /**
       * displays the camera for the user to select/take a photo
       */
      recordVideo: function recordVideo() {
        var q = $q.defer();

        //video options
        var options = {limit: 3, duration: 10};

        $cordovaCapture.captureVideo(options).then(function(videoData) {
          q.resolve(videoData);
        }, function(err) {
          q.reject(err);
        });

        return q.promise;
      }


    };
  });

})(angular.module('mbbc.common'));
