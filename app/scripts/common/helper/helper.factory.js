/**
 * @ngdoc service
 * @name guest.guest
 * @description
 * # guest
 * `guest` is a factory in the `guest` module.
 */
(function(module) {
  'use strict';

  module.factory('Helper', function($q, $ionicLoading, $ionicPopup) {

    return {


      /*
       * show regular ionic popup message
       * params : @title(string) | @message(string)
       */
      showAlert: function(_message, _title) {

        var q = $q.defer();

        $ionicPopup.alert({
          title: _title || "Alert",
          template: _message || "",
          buttons: [{
            text: "OK",
            type: "button-positive",
            onTap: function(e) {
              q.resolve(e);
            }
          }]
        });

        return q.promise;

      },


      /**
       * Format phone numbers
       * courtesy of http://snipplr.com/view/65672/
       */
      formatPhone: function(phonenum) {
        var regexObj = /^(?:\+?1[-. ]?)?(?:\(?([0-9]{3})\)?[-. ]?)?([0-9]{3})[-. ]?([0-9]{4})$/;
        if (regexObj.test(phonenum)) {
          var parts = phonenum.match(regexObj);
          var phone = "";
          if (parts[1]) {
            phone += "+1 (" + parts[1] + ") ";
          }
          phone += parts[2] + "-" + parts[3];
          return phone;
        } else {
          //invalid phone number
          return phonenum;
        }
      },



      /*
       * get expiration date from now
       */
      getExpDate: function(dateObj) {
        var exp = new Date(dateObj);
        var now = new Date();
        var timeDiff = exp.getTime() - now.getTime();
        if (timeDiff <= 0) {
          // clearTimeout(timer);
          // Run any code needed for countdown completion here
          return;
        }
        var seconds = Math.floor(timeDiff / 1000);
        var minutes = Math.floor(seconds / 60);
        var hours = Math.floor(minutes / 60);
        var days = Math.floor(hours / 24);
        hours %= 24;
        minutes %= 60;
        seconds %= 60;

        return {
          days: days,
          hours: hours,
          minutes: minutes,
          seconds: seconds
        };
      },

      /*
       * email validation function
       */
      validateEmail: function(email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
      },

      /*
       * text length limatation
       * @params - text(string), length(number) : limitation number
       */
      truncateText: function(text, length) {
        var txt = text.substring(0, length) + "...";
        return txt;
      }


    };
  });

})(angular.module('mbbc.common'));
