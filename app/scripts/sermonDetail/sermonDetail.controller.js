/**
 * @ngdoc function
 * @name guest.GuestController
 * @description
 * # GuestController
 * `GuestController` is a controller in the `guest` module.
 */
(function(module) {

  'use strict';

  module.controller('sermonDetailCtrl', function($scope, $q, $state, $stateParams, $sce) {

    $scope.passedData = JSON.parse($stateParams.item);

    $scope.videoUrl = $sce.trustAsResourceUrl(JSON.parse($stateParams.item).files[1].link);
    console.log(JSON.parse($stateParams.item));

    $scope.openSermon = function() {

      $state.go("tab.sermons");

    }

    //console.log(JSON.stringify($scope.passedData));

  });
})(angular.module('mbbc.sermonDetail'));
