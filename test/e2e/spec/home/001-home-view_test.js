describe('Verizon@Work: Home View', function () {
    'use strict';

    /* jshint -W117 */

    var HomePage, home;

    HomePage = require('../../framework/home/home.page');

    beforeEach(function () {
        home = new HomePage(browser);
        home.browser.get(home.url);
    });

    it('displays the home page with (at most) nine menu options', function () {
        expect(home.browser.getLocationAbsUrl()).toBe('/dashboard');
        home.menuOptions.get().count().then(function (count) {
            expect(count).toBeLessThan(10);
        });
    });

    it('displays search results with a typeahead dropdown', function () {
        home.searchInput.get().sendKeys('makam');

        home.typeaheadMatches.get().count().then(function (count) {
            expect(count).toBeGreaterThan(1);
            expect(count).toBe(2);
        });
    });
});
