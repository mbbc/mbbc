describe('eDirectory Mobile: Home', function () {
    'use strict';

    var results, options;
    //start search and get search results
    beforeEach(function () {
        browser.get('/#/home');

    });

    it('displays the home page with (at most) nine menu options', function () {
        expect(browser.getLocationAbsUrl()).toBe('/home');
        options = element.all(by.css('.vzatwork-menu-option'));
        options.count().then(function (count) {
            expect(count).toBe(9);
        });
    });

    it('displays search results with a dropdown', function () {
        element(by.model('search.term')).sendKeys('makam');
        results = element.all(by.repeater('match in matches'));
        results.count().then(function (count) {
            expect(count).toBeGreaterThan(1);
            expect(count).toBe(2);
        });
    });
});
