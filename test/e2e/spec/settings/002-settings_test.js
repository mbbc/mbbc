describe('Verizon@Work: Settings View', function () {
    'use strict';

    var HomePage, home, SettingsPage, settings, VZWEB, VZWEB_WIN;

    HomePage = require('../../framework/home/home.page');
    SettingsPage = require('../../framework/settings/settings.page');
    VZWEB = 0;
    VZWEB_WIN = 1;

    beforeEach(function () {
        settings = new SettingsPage(browser);
        settings.browser.get(settings.url);
        home = new HomePage(settings.browser);
    });

    it('allows users and testers to select "Production" as their environment', function () {
        settings.env.input.production.get().getAttribute('class').then(function (classes) {
            expect(classes).not.toContain('ng-valid-parse');
        });

        settings.env.label.production.get().click();
        settings.env.input.production.get().getAttribute('class').then(function (classes) {
            expect(classes).toContain('ng-valid-parse');
        });
        settings.env.input.uat.get().getAttribute('class').then(function (classes) {
            expect(classes).not.toContain('ng-valid-parse');
        });
    });

    it('changes all settings to use "UAT" resources and API endpoints', function () {
        var smTargetVal = '$SM$HTTPS%3a%2f%2fvzweb2%2eebiz%2everizon%2ecom%2f';
        settings.env.label.uat.get().click();

        home.browser.get(home.url);
        home.menuOptions.get().then(function (options) {
            options[VZWEB].click().then(function () {
                home.browser.getAllWindowHandles().then(function (handles) {
                    home.browser.switchTo().window(handles[VZWEB_WIN]).then(function () {
                        home.browser.ignoreSynchronization = true;
                        expect(home.browser.getCurrentUrl()).toContain(smTargetVal);
                        home.browser.ignoreSynchronization = false;
                    });
                });
            });
        });

    });
});

