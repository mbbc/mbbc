describe('Verizon@Work: Authentication View', function () {
    'use strict';

    var SsoPage, props, sso, ssoUrl;

    SsoPage = require('../../framework/auth/sso.page');
    props = require('../../framework/auth/sso.properties');

    /* jshint -W117 */

    //start search and get search results
    beforeEach(function () {
        ssoUrl = 'https://logintpa.verizon.com/onepassword/onelogin.jsp?CHALLENGE=&SMAGENTNAME=$SM$o12G5FgALgMdLMUW04Zv%2f4hBKVrBJz9H%2fNk8Zy%2b8lmbpsAfjivK6A7hUoCIl%2ffb3&TARGET=$SM$HTTPS%3a%2f%2faboutyou%2everizon%2ecom%2f%3fvzatwork%3d1';
        sso = new SsoPage(browser, ssoUrl);
        sso.browser.ignoreSynchronization = true;
        sso.authenticate(props.values.userId, props.values.password, true);
    });

    afterEach(function () {
        sso.browser.ignoreSynchronization = false;
    });

    it('logs users in', function () {
        expect(sso.browser.getCurrentUrl()).toBe('https://aboutyou.verizon.com/?vzatwork=1');
    });

});
