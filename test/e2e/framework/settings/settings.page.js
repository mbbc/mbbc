(function () {
    'use strict';

    var SettingsPage, props;

    props = require('./settings.properties');

    SettingsPage = function (browser) {
        this.browser = browser;

        this.url = props.urls.lcl;

        this.env = {
            input: {
                production: {
                    get: function () {
                        return element(by.xpath(props.xpath.env.input.prd));
                    }
                },
                uat       : {
                    get: function () {
                        return element(by.xpath(props.xpath.env.input.uat));
                    }
                },
                all       : {
                    get: function () {
                        return element.all(by.xpath(props.xpath.env.input.all));
                    }
                }
            },
            label: {
                production: {
                    get: function () {
                        return element(by.xpath(props.xpath.env.label.prd));
                    }
                },
                uat       : {
                    get: function () {
                        return element(by.xpath(props.xpath.env.label.uat));
                    }
                },
                all       : {
                    get: function () {
                        return element.all(by.xpath(props.xpath.env.label.all));
                    }
                }
            }
        }
    };

    module.exports = SettingsPage;
})();