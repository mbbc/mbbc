(function () {
    'use strict';

    var SettingsProperties, absUrl;

    absUrl = '/#/settings';

    SettingsProperties = {
        xpath: {
            env: {
                input: {
                    prd: './/input[@value="prd"]',
                    uat: './/input[@value="uat"]',
                    all: './/input[@name="vzatwork-settings-env"]'
                },
                label: {
                    prd: './/label[@value="prd"]',
                    uat: './/label[@value="uat"]',
                    all: './/label[@name="vzatwork-settings-env"]'
                }
            }
        },
        urls : {
            lcl: absUrl,
            sit: absUrl,
            uat: absUrl,
            prd: absUrl
        }
    };

    module.exports = SettingsProperties;
})();