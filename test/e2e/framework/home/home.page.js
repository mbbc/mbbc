(function () {
    'use strict';

    /* jshint -W117 */

    var HomePage, props;

    props = require('./home.properties');

    HomePage = function (browser) {
        this.browser = browser;

        this.url = props.urls.lcl;

        this.searchInput = {
            get: function () {
                return element(by.model(props.model.searchInput));
            }
        };

        this.menuOptions = {
            get: function () {
                return element.all(by.css(props.css.menuOptions));
            }
        };

        this.typeaheadMatches = {
            get: function () {
                return element.all(by.repeater(props.repeater.matches));
            }
        };

        this.footer = {
            feedback  : {
                get: function () {
                    return element(by.id(props.id.footer.feedback));
                }
            },
            settings  : {
                get: function () {
                    return element(by.id(props.id.footer.settings));
                }
            },
            disclaimer: {
                get: function () {
                    return element(by.id(props.id.footer.disclaimer));
                }
            }
        }

    };

    module.exports = HomePage;
})();