(function () {
    'use strict';

    var HomeProperties, absUrl;

    absUrl = '/#/home';

    HomeProperties = {
        id      : {
            footer: {
                feedback  : 'vzatwork-footer-feedback',
                settings  : 'vzatwork-footer-settings',
                disclaimer: 'vzatwork-footer-disclaimer'
            }
        },
        model   : {
            searchInput: 'search.term'
        },
        css     : {
            menuOptions: '.button-circle'
        },
        repeater: {
            matches: 'match in matches'
        },
        urls    : {
            lcl: absUrl,
            sit: absUrl,
            uat: absUrl,
            prd: absUrl
        }
    };

    module.exports = HomeProperties;
})();