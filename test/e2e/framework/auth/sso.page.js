(function () {
    'use strict';

    /* jshint -W117 */

    var SsoPage, props;

    props = require('./sso.properties');

    SsoPage = function (browser, url) {
        var ssoPageInstance = this;

        this.browser = browser;

        this.url = url;

        this.userId = {
            get: function () {
                return element(by.xpath(props.xpath.userId));
            }
        };
        this.password = {
            get: function () {
                return element(by.xpath(props.xpath.password));
            }
        };
        this.signOn = {
            get: function () {
                return element(by.xpath(props.xpath.signOnBtn));
            }
        };
        this.alert = {
            dialog : null,
            get    : function () {
                this.dialog = ssoPageInstance.browser.switchTo().alert();
                return this.dialog;
            },
            accept : function () {
                this.dialog.get().accept();
            },
            dismiss: function () {
                this.dialog.get().dismiss();
            }
        };
        this.authenticate = function (userId, password, acceptTerms) {
            acceptTerms = (acceptTerms || true);
            this.browser.get(this.url);
            this.userId.get().sendKeys(userId);
            this.password.get().sendKeys(password);
            this.signOn.get().click();
            if (acceptTerms) {
                this.alert.get().accept();
            }
            else {
                this.alert.get().dismiss();
            }
        };
    };

    module.exports = SsoPage;
})();