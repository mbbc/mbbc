(function () {
    'use strict';

    module.exports = {
        xpath : {
            userId   : './/*[@id="USERID"]',
            password : './/*[@id="PASSWORD"]',
            signOnBtn: './/*[@id="domainLogin"]/div[5]/a'
        },
        id    : {},
        model : {},
        css   : {},
        urls  : {
            lcl: '',
            sit: '',
            uat: '',
            prd: 'https://logintpa.verizon.com/onepassword/onelogin.jsp?CHALLENGE=&SMAGENTNAME=$SM$o12G5FgALgMdLMUW04Zv%2f4hBKVrBJz9H%2fNk8Zy%2b8lmbpsAfjivK6A7hUoCIl%2ffb3&TARGET=$SM$HTTPS%3a%2f%2faboutyou%2everizon%2ecom%2f%3fvzatwork%3d1'
        },
        values: {
            userId  : 'v128477',
            password: '?pnewR0vO'
        }
    };
})();