(function () {
    'use strict';

    module.exports = {
        xpath: {},
        id   : {},
        model: {},
        css  : {},
        urls : {
            lcl: '',
            sit: '',
            uat: '',
            prd: ''
        }
    };
})();