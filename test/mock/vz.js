var vz = (function (window) {
    var verizon = vz || {};

    verizon.namespace = function (namespace) {
        var namespaces, root, i;
        namespaces = namespace.split('.');
        root = window;

        for (i = 0; i < namespaces.length; i++) {
            if (!root[namespaces[i]]) {
                root[namespaces[i]] = {};
            }
            root = root[namespaces[i]];
        }
    };

    return verizon;
}(window));
