vz.namespace('vz.test.mock');
vz.test.mock.jasmineHelper = (function (undefined) {

    var isHttpBackendDefined = !angular.isUndefined(vz.test.mock.$httpBackend);

    var commonMocks = {
        cookies: {
            SMSESSION: true
        },
        href   : 'http://localhost.verizon.com/#/tab/search',
        $window: {},
        window : {
            cordova  : {
                plugins: {
                    Keyboard: {
                        hideKeyboardAccessoryBar: function (hide) {
                            /* jshint -W030 */
                            hide || (hide = false);
                        }
                    },
                    printer : {
                      print : function(response){
                        response || (response = false)
                      }
                    }
                }
            },
            location : {
                path   : function () {
                },
                replace: function () {
                    return true;
                }
            },
            StatusBar: {
                styleDefault: function () {
                    return true;
                }
            },
            document : window.document,
            navigator: window.navigator
        },
        cordova: {
            plugins: {
                Keyboard: {
                    hideKeyboardAccessoryBar: function (hide) {
                        /* jshint -W030 */
                        hide || (hide = false);
                    },
                    close :  function(){}
                }
            }
        }
    };

    return {
        afterEach : function () {
            if (isHttpBackendDefined) {
                try {
                    vz.test.mock.$httpBackend.flush();
                }
                catch (ignore) {
                }
                vz.test.mock.$httpBackend.verifyNoOutstandingExpectation();
                vz.test.mock.$httpBackend.verifyNoOutstandingRequest();
            }
        },
        beforeEach: function ($httpBackend) {
            if (isHttpBackendDefined) {
                vz.test.mock.$httpBackend = $httpBackend;
                vz.test.mock.$httpBackend.whenGET(/^.*\.(html)$/).respond('<html></html>');
                $httpBackend.whenGET(/(apps\/profilepantry)?\/profiles\/(0|\d{10})\/vzAtWorkEntitlements/).respond(vz.test.fixture.json.entitlements.restfulget.doemsticAll);
                $httpBackend.whenGET(/apps\/todos\/todo\?showtodo/).respond(vz.test.fixture.json.todos.restfulget.mytodos);

            }
        },
        init      : function () {
            beforeEach(module(function ($provide) {
                $provide.value('$cookies', commonMocks.cookies);
                $provide.value('$window', commonMocks.window);
                $provide.value('$anchorScroll', commonMocks.$anchorScroll);
            }));
        }
    };
})();

vz.test.mock.jasmineHelper.init();
