vz.namespace('vz.test.mock');
vz.test.mock.$httpBackend = {
    flush                         : function () {
    },
    when                          : function () {
        return {
            respond: function () {
            }
        }
    },
    verifyNoOutstandingExpectation: function () {
    },
    verifyNoOutstandingRequest    : function () {
    }
};