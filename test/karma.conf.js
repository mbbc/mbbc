// Karma configuration
// http://karma-runner.github.io/0.12/config/configuration-file.html
// Generated on 2014-09-03 using
// generator-karma 0.8.3

module.exports = function (config) {
    // 'use strict';

    config.set({
        basePath: '../',

        frameworks      : [
            'jasmine'
        ],
        files           : [
          'app/bower_components/angular/angular.js',
          'app/bower_components/angular-mocks/angular-mocks.js',
          'app/bower_components/ngCordova/dist/ng-cordova-mocks.js',
            // bower:js
            'app/bower_components/angular-animate/angular-animate.js',
            'app/bower_components/angular-sanitize/angular-sanitize.js',
            'app/bower_components/angular-ui-router/release/angular-ui-router.js',
            'app/bower_components/ionic/release/js/ionic.js',
            'app/bower_components/ionic/release/js/ionic-angular.js',
            'app/bower_components/ngCordova/dist/ng-cordova.js',
            'app/bower_components/angular-scenario/angular-scenario.js',
            // endbower

            'app/scripts/**/*.module.js',
            'app/scripts/**/!(*_test).js',
            //'app/components/**/*.module.js',
            //'app/components/**/!(*_test).js',

            //'app/*.js',
            'test/mock/vz.js',
          //  'test/mock/fixture/**/*.js',
            'test/mock/vz.test.mock.$httpBackend.js',
            'test/mock/vz.test.mock.jasmineHelper.js',
            'app/**/**/*_test.js',

        ],
        // level of logging
        // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
        logLevel: config.LOG_INFO,
        exclude         : [
          'app/scripts/app.js'
        ],
        autoWatch       : true,
        // Start these browsers, currently available:
        // - Chrome
        // - ChromeCanary
        // - Firefox
        // - Opera
        // - Safari (only Mac)
        // - PhantomJS
        // - IE (only Windows)
        browsers: [
          "PhantomJS",
        //"Chrome"
        ],
        // Which plugins to enable
        plugins: [
          "karma-jasmine",
          "karma-chrome-launcher",
          "karma-phantomjs-launcher",
          "karma-coverage",
          "karma-junit-reporter"
        ],
        reporters       : [
            'progress',
            'coverage',
            'junit'
        ],
        // Continuous Integration mode
        // if true, it capture browsers, run tests and exit
        singleRun: false,

        colors: true,

        // web server port
        port: 8080,
        preprocessors   : {
            // Update this if you change the yeoman config path
            'app/*.js': ['coverage'],
            'app/scripts/**/!(*_test).js': ['coverage'],
            //'app/components/**/!(*_test).js': ['coverage'],
            '**/*.html': ['ng-html2js']
        },
        coverageReporter: {
            reporters: [
                {type: 'html', dir: 'test/reports/coverage/'},
                {type: 'text-summary'},
                {type: 'text'}
            ]
        },
        junitReporter   : {
            outputFile: 'test/reports/testoutput/test-results.xml'
        }
    });
};
