var HtmlReporter = require('protractor-html-screenshot-reporter');

var reporter = new HtmlReporter({
    baseDirectory: './test/reports/e2e', // a location to store screen shots.
    docTitle     : 'eDirectory Mobile | E2E/Functional Test Results',
    docName      : 'index.html'
});

exports.config = {
    allScriptsTimeout: 300000,
    getPageTimeout   : 300000,
    //seleniumServerJar: '../node_modules/protractor/selenium/selenium-server-standalone-2.47.1.jar',
    //seleniumPort: 4444,

    // localhost
    seleniumAddress: 'http://localhost.verizon.com:4444/wd/hub',

    // local Appium
    //seleniumAddress: 'http://localhost.verizon.com:4723/wd/hub',

    // Verizon enterprise Selenium hub
    //seleniumAddress: 'http://10.77.41.47:4444/wd/hub',

    specs: [
        './e2e/spec/**/*_test.js'
    ],

    capabilities: {
        'browserName': 'firefox'
    },

    /*capabilities: {
     browserName     : 'safari',
     'appium-version': '1.4.8',
     platformName    : 'iOS',
     platformVersion : '8.2',
     deviceName      : 'iPhone 6'
     },*/

    // Change this to your local IP *OR*
    baseUrl: 'http://143.91.64.78:8100/',

    framework: 'jasmine',

    jasmineNodeOpts: {
        showColors            : true,
        defaultTimeoutInterval: 300000,
        isVerbose             : false,
        includeStackTrace     : false
    },

    onPrepare: function () {
        jasmine.getEnv().addReporter(reporter);
    }

    // configuring wd in onPrepare
    // wdBridge helps to bridge wd driver with other selenium clients
    // See https://github.com/sebv/wd-bridge/blob/master/README.md
    /*onPrepare: function () {
     var wd         = require('wd'),
     protractor = require('protractor'),
     wdBridge   = require('wd-bridge')(protractor, wd);
     wdBridge.initFromProtractor(exports.config);
     jasmine.getEnv().addReporter(reporter);
     }*/
};
